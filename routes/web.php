<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/form', 'FormController@index');
Route::post('/form', 'FormController@post');

Route::get('/hello', 'HelloController@index');

Route::get('/hello/{user}.json', 'HelloController@userAsJson');

Route::get('/hello/{user}', 'HelloController@user');

//Route::get('/hello/{user}/test', function ($user) {
//    return redirect('/');
//});

//Route::get('/test', function ($user) {
//    return '<h1>Hello test ' . $user . '</h1>';
//});
//
//Route::get('/json', function () {
//    return ['status' => 'ok', 'date' => date('Y-m-d H:i:s')];
//});

//Route::get('/articles', 'ArticleController@index');
//Route::get('/articles/{slug}', 'ArticleController@show');
//Route::get('/articles/{slug}/comments', 'ArticleController@showComments');
//
//Route::post('/articles/{slug}/comments', 'ArticleController@addComment');

