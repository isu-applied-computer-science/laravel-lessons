@extends('layouts/app')

@section('title', 'HELLO USER')

{{--@section('pre-content', 'Это перед всем содержимым страницы')--}}

@section('content')

    <h1>Hello {{$user}}!!!</h1>

    <p>Template update!</p>


    <ul>
    @foreach ($items as $item)
        @continue($item === 'third')
        <li>
            {{$loop -> iteration}}/{{$loop -> count}} . {{ $item }}
            @if($loop->first)
            (first!)
            @endif
            @if($loop->last)
            (last!)
            @endif
            @if($loop->even)
            четныц
            @endif
            @if($loop->odd)
            нечетный
            @endif
            - осталось: {{ $loop -> remaining}}
        </li>

        @break($item === 'fourth')
    @endforeach
    </ul>

    @if($numbers)
        <ul>
        @foreach ($numbers as $number)
            <li>{{ $number }}</li>
        @endforeach
        </ul>
    @else
        <p>Number is empty!</p>
    @endif


    @section('inside')
        <p> I am insider!</p>
    @show

@endsection
