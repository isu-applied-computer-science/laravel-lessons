<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
</head>
<body>

    <div>
        LOGO
        <br>
        @include('header')
    </div>

    <hr>

    {{-- <div>
        @section('pre-content')
            Это Default для начала страницы
        @show
    </div>

    <hr> --}}

    <div>@yield('content')</div>


    <hr>

    <div>Footer</div>

</body>
</html>
