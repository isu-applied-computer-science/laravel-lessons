@extends('layouts.app')

@section('content')
<form method="POST">
    @csrf
    <div>
        <label>Name</label>
        <br>
        <input type="text" name="name" value="{{ old('name') }}">
        @error('name')
        <div>{{$message }}</div>
        @enderror
    </div>
    <div>
        <label>City</label>
        <br>
        <input type="text" name="city" value="{{ old('city') }}">
        @error('city')
        <div>{{$message }}</div>
        @enderror
    </div>
    <div>
        <label>Age</label>
        <br>
        <input type="text" name="age" value="{{ old('age') }}">
        @error('age')
        <div>{{$message }}</div>
        @enderror
    </div>
    <div>
        <button>submit</button>
    </div>
</form>
@endsection
