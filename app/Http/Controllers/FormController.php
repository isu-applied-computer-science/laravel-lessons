<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index()
    {
        return view('form');
    }

    public function post(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'city' => 'required',
            'age' => 'required'
        ]);

        $name = $request->name;
        $city = $request->city;
        $age = $request->age;

        return 'Hello, ' . $name . '! You are from ' . $city . ', and you are ' . $age . ' y.o.';
    }
}
