<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelloController extends Controller
{
    public function index()
    {
        return '<h1>Hello world!</h1>';
    }

    public function user($user)
    {
        $items = [
            'first',
            'second',
            'third',
            'fourth',
            'fifth',
        ];

        $numbers = [];

        for ($i=0; $i < 10; $i++) {
            $numbers[] = $i;
        }

        return view('hello-user', compact('user', 'items', 'numbers'));
        // $test = 'я тест';
        // $product = 'Holodilnik';
        // return view('hello-user', compact(
        //     'user', 'test', 'product'
        // ));
    }

    public function userAsJson()
    {
        return ['status' => 'ok', 'date' => date('Y-m-d H:i:s')];
    }
}
